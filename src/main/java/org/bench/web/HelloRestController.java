package org.bench.web;

import org.bench.DemoApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/*
 * Controller + ResponseBody -> returning data rather than a view
 */
@RestController
public class HelloRestController {

	@RequestMapping("/")
	public String index() {
		return "Greetings from Spring Boot!";
	}

	@RequestMapping("/kill")
	public String shutDown() {
		Thread th = new Thread() {
			public void run() {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				DemoApplication.closeApplication();
			};
		};
		th.start();
		return "The Application is about to shut down...";
	}
}
