package org.bench;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

/*
 * Configuration + EnableAutoConfiguration 
 */
@SpringBootApplication
public class DemoApplication {

	private static ConfigurableApplicationContext run;

	public static void main(String[] args) {
		run = SpringApplication.run(DemoApplication.class, args);
	}

	public static void closeApplication() {
		/*
		 * The "endpoints.shutdown.enabled=true" did not work, so this is a
		 * quick hack.
		 */
		System.out.println("Before close: " + run.isActive());
		run.close();
		System.out.println("Afer close: " + run.isActive());
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			String name = ctx.getApplicationName();
			System.out.println("App Name: " + name);
			// System.out.println("Let's inspect the beans provided by Spring
			// Boot:");
			//
			// String[] beanNames = ctx.getBeanDefinitionNames();
			// Arrays.sort(beanNames);
			// for (String beanName : beanNames) {
			// System.out.println(beanName);
			// }
		};
	}
}
